package com.example.moduletenpartone;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

public class CarActivity extends AppCompatActivity {
    private ImageView mCarImageView;
    private TextView mNameCar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car);

        mNameCar = findViewById(R.id.nameCar);
        mCarImageView = findViewById(R.id.carImageView);

        String name = getIntent().getExtras().getString("carName");
        int position = Integer.parseInt(getIntent().getExtras().getString("position"));

        int[] arrayColor = getResources().getIntArray(R.array.nameCarColor);
        TypedArray arrayImage = getResources().obtainTypedArray(R.array.carsImage);

        mNameCar.setTextColor(arrayColor[position]);
        mNameCar.setText(name);
        mCarImageView.setImageResource(arrayImage.getResourceId(position, -1));

    }
}
