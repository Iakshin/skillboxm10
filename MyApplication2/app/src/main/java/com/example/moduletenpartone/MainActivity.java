package com.example.moduletenpartone;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity {
    private Spinner mSpinnerChooseCar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSpinnerChooseCar = findViewById(R.id.spinnerChooseCar);

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_AUTO_BATTERY);
   //     AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_AUTO);
    }

    public void chooseCar(View view) {
        Intent intent = new Intent(MainActivity.this, CarActivity.class);
        intent.putExtra("carName", mSpinnerChooseCar.getSelectedItem().toString());
        intent.putExtra("position", String.valueOf(mSpinnerChooseCar.getSelectedItemPosition()));

        startActivity(intent);
    }
}
