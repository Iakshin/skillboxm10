package com.example.moduletenparttwo;

public class Song {
    private int id;
    private String nameSong;
    private String nameArtist;
    private String nameAlbum;
    private int duration;

    public Song(int id, String nameSong, String nameArtist, String nameAlbum, int duration) {
        this.id = id;
        this.nameSong = nameSong;
        this.nameArtist = nameArtist;
        this.nameAlbum = nameAlbum;
        this.duration = duration;
    }

    public int getId() {
        return id;
    }

    public String getNameSong() {
        return nameSong;
    }

    public String getNameArtist() {
        return nameArtist;
    }

    public String getNameAlbum() {
        return nameAlbum;
    }

    public int getDuration() {
        return duration;
    }
}
