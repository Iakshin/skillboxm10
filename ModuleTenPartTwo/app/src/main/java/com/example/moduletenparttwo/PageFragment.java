package com.example.moduletenparttwo;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;

public class PageFragment extends Fragment {

    private ImageView mAlbumImage;


    public static Fragment newInstance(String nameAlbum) {
        PageFragment fragment = new PageFragment();
        Bundle arg = new Bundle();
        arg.putString("AlbumName", nameAlbum);
        fragment.setArguments(arg);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.informatiot_about_singl_fragment, null);
        mAlbumImage = view.findViewById(R.id.albumImage);
        int resID = getResources().getIdentifier(getArguments().getString("AlbumName") , "drawable" ,
                view.getContext().getPackageName()) ;
        mAlbumImage.setImageResource(resID);

        return view;
    }
}
