package com.example.moduletenparttwo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.graphics.drawable.Drawable;
import android.graphics.drawable.DrawableContainer;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

//как то тяжкобез stream и lambd
public class MainActivity extends AppCompatActivity {
    private MediaPlayer mediaPlayer;

    private List<Song> listSong;
    private boolean state;
    private int activeSong;
    private int durationActiveSong;

    //не красиво как то, но как малой кровью еще обойтись я не придумал
    private boolean triggerSeekBar;

    private Handler thread;

    private SeekBar mTimeSeekBar;
    private TextView mPassedFromSong;
    private TextView mLeftFromSong;
    private TextView mNameSong;
    private TextView mNameArtist;
    private ViewPager mAlbumPager;
    private ImageButton mStateButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM);

        thread = new Handler();

        mStateButton = findViewById(R.id.stateSongButton);
        mNameSong = findViewById(R.id.nameSong);
        mNameArtist = findViewById(R.id.nameArtist);
        mPassedFromSong = findViewById(R.id.passedFromSong);
        mLeftFromSong = findViewById(R.id.leftFromSong);
        mTimeSeekBar = findViewById(R.id.timeSeekBarSong);

        mTimeSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {


            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                triggerSeekBar = false;
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                mediaPlayer.seekTo(seekBar.getProgress());
                triggerSeekBar = true;
            }
        });


        listSong = new ArrayList<>();

        Field[] fields = R.raw.class.getFields();
        try {
            for (Field field : fields) {
                String fileName = field.getName();
                Uri uri = Uri.parse("android.resource://" + getPackageName() + "/raw/" + fileName);

                int id = field.getInt(field);

                MediaMetadataRetriever retriever = new MediaMetadataRetriever();
                retriever.setDataSource(getApplication(), uri);
                String nameSong = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
                String nameArtist = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
                String nameAlbum = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM);
                int duration = Integer.valueOf(retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION));
                Song song = new Song(id, nameSong, nameArtist, nameAlbum, duration);
                listSong.add(song);
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        mAlbumPager = findViewById(R.id.albumPager);

        CustomPagerAdapter adapter = new CustomPagerAdapter(getSupportFragmentManager());
        //       mAlbumPager.setPageTransformer(true, new ZoomOutPagerTransformer());
        mAlbumPager.setAdapter(adapter);

        mAlbumPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                activeSong = position;
                preparationSong(activeSong);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        state = false;
        triggerSeekBar = true;

        activeSong = 0;
        preparationSong(activeSong);

    }

    private void preparationSong(int i) {
        if (mediaPlayer != null) {
            mediaPlayer.reset();
        }

        Song song = listSong.get(i);
        mNameSong.setText(song.getNameSong());
        mNameArtist.setText(song.getNameArtist());
        mediaPlayer = MediaPlayer.create(this, song.getId());

        durationActiveSong = song.getDuration();
        mTimeSeekBar.setMax(durationActiveSong);
        mLeftFromSong.setText(convertMillisec(durationActiveSong));
        if (state)
            play();
        thread.postDelayed(new TimeUpdate(), 50);


    }


    public void previousSongClick(View view) {
        if (--activeSong >= 0) {
            preparationSong(activeSong);
            mAlbumPager.setCurrentItem(activeSong, true);
        } else {
            ++activeSong;
        }

    }

    public void nextSongClick(View view) {
        if (++activeSong < listSong.size()) {
            preparationSong(activeSong);
            mAlbumPager.setCurrentItem(activeSong, true);
        } else {
            --activeSong;
        }


    }

    public void stateSongClick(View view) {
        if (!state) {
            mediaPlayer.start();
            mStateButton.setBackgroundResource(R.drawable.ic_pause_circle_filled_black_24dp);
        } else {
            mediaPlayer.pause();
            mStateButton.setBackgroundResource(R.drawable.ic_play_circle_filled_black_24dp);
        }
        state = !state;
    }

    private void play() {
        if (mediaPlayer != null) {
            mediaPlayer.start();
        }
    }

    class TimeUpdate implements Runnable {

        @Override
        public void run() {
            int time = mediaPlayer.getCurrentPosition();
            String passedFromSong = convertMillisec(time);
            if (triggerSeekBar) {
                mTimeSeekBar.setProgress(time);
            }
            mPassedFromSong.setText(passedFromSong);

            if (time >= durationActiveSong) {
                if (activeSong < listSong.size()) {
                    preparationSong(++activeSong);
                }
                thread.removeCallbacks(this);
            }

            thread.postDelayed(this, 50);
        }
    }

    private String convertMillisec(int millisec) {
        int minutes = millisec / 60000;
        int seconds = (millisec / 1000) % 60;
        return String.format("%02d : %02d", minutes, seconds);
    }

    private class CustomPagerAdapter extends FragmentPagerAdapter {

        public CustomPagerAdapter(FragmentManager fm) {
            super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        }

        @Override
        public Fragment getItem(int position) {
            return PageFragment.newInstance(listSong.get(position).getNameAlbum());
        }

        @Override
        public int getCount() {
            return listSong.size();
        }

    }
}
